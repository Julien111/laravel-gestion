<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\TaskTypeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

// partie profil
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/profil', [ProfilController::class, 'index'])->name('profil.index');

//création et affichage des tâches model task-type

Route::get('/task-type', [TaskTypeController::class, 'index'])->name('taskType.index');
Route::get('/task-type/create', [TaskTypeController::class, 'create'])->name('taskType.create');
Route::get('/task-type/show/{taskType}', [TaskTypeController::class, 'show'])->name('taskType.show');

Route::post('/task-type/create', [TaskTypeController::class, 'store'])->name('taskType.store');

Route::get('/task-type/{taskType}/edit', [TaskTypeController::class, 'edit'])->name('taskType.edit');
Route::post('/task-type/{taskType}', [TaskTypeController::class, 'update'])->name('taskType.update');
Route::delete('/task-type/destroy/{taskType}', [TaskTypeController::class, 'destroy'])->name('taskType.destroy');

//fin model task-type

//partie gestion des tâches

Route::get('/tasks', [TasksController::class, 'index'])->name('tasks.index');
Route::get('/tasks/create', [TasksController::class, 'create'])->name('tasks.create');
Route::get('/tasks/show/{tasks}', [TasksController::class, 'show'])->name('tasks.show');

Route::post('/tasks/create', [TasksController::class, 'store'])->name('tasks.store');

Route::get('/tasks/edit/{id}', [TasksController::class, 'edit'])->name('tasks.edit');
Route::post('/tasks/update/{id}', [TasksController::class, 'update'])->name('tasks.update');

Route::delete('/tasks/destroy/{tasks}', [TasksController::class, 'destroy'])->name('tasks.destroy');

//fin partie profil