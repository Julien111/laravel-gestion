<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    use HasFactory;

    protected $fillable = ['etat', 'created', 'task_type_id', 'user_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function taskType()
    {
        return $this->belongsTo(TaskType::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}