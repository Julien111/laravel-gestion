<?php

namespace App\Http\Controllers;

use App\Models\TaskType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TaskTypeController extends Controller
{
    /**
     * Display a listing of the task-type model.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $taskTypes = TaskType::all();
        return view('taskType.index', compact('taskTypes','taskTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        return view('taskType.create');
    }

    /**
     * Store a new task-type element in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $request->validate([
            'name'=> 'required|string|max:255',
        ]);

        $taskType = TaskType::create([
            'name' => $request->name,
        ]);
        $taskType->save();

        return to_route('taskType.index')->with('success','Votre tâche a été ajouté.');
    }

    /**
     * Display the task-type resource.
     *
     * @param  \App\Models\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function show(TaskType $taskType)
    {

        if (! Gate::allows('access-user')) {
            abort(403);
        }

        return view('taskType.show', compact('taskType'));
    }

    /**
     * Show the form for editing the task-type resource.
     *
     * @param  \App\Models\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskType $taskType)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }
        return view("taskType.edit", compact("taskType"));
    }

    /**
     * Update the task-type resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskType $taskType)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $request->validate = [
            'name' => 'required|string|max:255',
        ];

        $taskType->update($request->all());

        return redirect()->route('taskType.index')->with('success', 'Tâche mise à jour avec succès.');
    }

    /**
     * Remove the task-type resource from storage.
     *
     * @param  \App\Models\TaskType  $taskType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskType $taskType)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $taskType->delete();
        return redirect()->route('taskType.index')->with('success', "L'élément a été supprimé.");
    }
}