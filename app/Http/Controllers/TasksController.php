<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Tasks;
use App\Models\TaskType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (! Gate::allows('access-user')) {
            abort(403);
        }
        $user = Auth::user();
        $tasks = Tasks::where("user_id", $user->id)->get();

        return view('tasks.index', compact('tasks','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $taskTypes = TaskType::all();

        return view('tasks.create', compact('taskTypes', 'taskTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        //vérifier le contenu partie état
        $testValues = array("À faire", "Terminée", "En attente");
        if(!in_array($request->etat, $testValues)){
            return redirect()->route('tasks.create')->with('message-error',"Le formulaire n'est pas valide. Le statut est incorrect.");
        }
        else{

            //vérifier la partie id (task-type)
            $taskTypes = TaskType::all();
            $control = "";

            foreach($taskTypes as $taskType){
                if($taskType->id == $request->task_type_id){
                    $control = "past control";
                }
            }

            if($control === "past control"){

                $request->validate([
                    'etat'=> 'required|string|max:255',
                    'task_type_id' =>"required",
                ]);

                $user = Auth::user()->id;
                $date = Carbon::now()->format('Y-m-d');

                $tasks = Tasks::create([
                    'etat' => $request->etat,
                    'created' => $date,
                    'task_type_id' => $request->task_type_id,
                    'user_id' => $user,
                ]);
                $tasks->save();

                return to_route('tasks.index')->with('message','Votre tâche a été ajoutée à votre liste.');
            }
            else{
                return to_route('tasks.create')->withErrors(["Le formulaire n'est pas valide. La tâche est incorrecte."]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function show(Tasks $tasks)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        return view('tasks.show', compact('tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }
        $taskTypes = TaskType::all();

        $tasks = Tasks::findOrFail($id);

        return view("tasks.edit", compact("tasks", "taskTypes"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {

        if (! Gate::allows('access-user')) {
            abort(403);
        }

        //début
        //vérifier le contenu partie état
        $testValues = array("À faire", "Terminée", "En attente");
        if(!in_array($request->etat, $testValues)){
            return redirect()->route('tasks.edit')->with('message-error',"Le formulaire n'est pas valide. Le statut est incorrect.");
        }
        else{
            //vérifier la partie id (task-type)
            $taskTypes = TaskType::all();
            $control = "";

            foreach($taskTypes as $taskType){
                if($taskType->id == $request->task_type_id){
                    $control = "past control";
                }
            }

            if($control === "past control"){

                $request->validate([
                    'etat'=> 'required|string|max:255',
                    'task_type_id' =>"required",
                ]);

                Tasks::find($id)->update($request->all());

                return to_route('tasks.index')->with('message','Votre tâche a été modifiée.');
            }
            else{
                return to_route('tasks.edit')->withErrors(["Le formulaire n'est pas valide. La tâche est incorrecte."]);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tasks  $tasks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tasks $tasks)
    {
        if (! Gate::allows('access-user')) {
            abort(403);
        }

        $tasks->delete();

        return redirect()->route('tasks.index')->with('success', "L'élément a été supprimé.");
    }
}
