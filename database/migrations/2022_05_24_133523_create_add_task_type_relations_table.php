<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {

            //On crée notre cle étrangère en bbd sur annonces
            $table->unsignedBigInteger('task_type_id');

            $table->foreign('task_type_id')->references('id')->on('task_types')->onDelete('cascade');

            Schema::enableForeignKeyConstraints();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //methode si on supprime notre cle étrangère
            Schema::disableForeignKeyConstraints();
            $table->dropForeign(['task_type_id']);
            $table->dropColumn('task_type_id');
        });
    }
};