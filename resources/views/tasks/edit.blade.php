@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')
@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@if(session()->has('message-error'))
    <div class="msg-error">
        <h4>{{ session()->get('message-error') }}</h4>
    </div>
@endif
<h2 class="title-task">Modifier la tâche</h2>
<div class="page-task-form">
<div class="container-form-task">
    <form action="{{ route('tasks.update', $tasks->id) }}" method="POST">
        @csrf
        <div id="form-task">
            <div class="block-form-task">
                <label for="taskType">Choisir votre tâche:</label>
                <select name="task_type_id" id="tasks-form-task">
                    @foreach ($taskTypes as $taskType)
                        @if ($tasks->task_type_id === $taskType->id)
                            <option value="{{ $tasks->taskType->id }}" selected>{{ $tasks->taskType->name }}</option>
                        @else
                            <option value="{{ $taskType->id }}">{{ $taskType->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="block-form-task">
                <label for="etat">Statut de la tâche :</label>
                <select name="etat" id="tasks-form-etat">
                    @if ($tasks->etat === "À faire")
                        <option value="À faire" selected>À faire</option>
                    @else
                        <option value="À faire">À faire</option>
                    @endif
                    @if ($tasks->etat === "En attente")
                        <option value="En attente" selected>En attente</option>
                    @else
                        <option value="En attente">En attente</option>
                    @endif
                    @if ($tasks->etat === "Terminée")
                        <option value="Terminée" selected>Terminée</option>
                    @else
                        <option value="Terminée">Terminée</option>
                    @endif
                </select>
            </div>
            <div>
                <button type="submit" class="btn-form-task">Modifier</button>
            </div>
        </div>
    </form>
</div>
<div id="task-btn-return">
    <div><a href="{{ route("tasks.index") }}" class="btn-task-return">Retour</a></div>
</div>
</div>
@endsection
