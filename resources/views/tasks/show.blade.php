@extends('layouts/base')

@section('content')
@include('components-nav.navProfil')

<div>
    <h2 class="title-task">Votre tâche</h2>
    <div class="page-task-form">
        <div class="container-form-task-show">
            <div>
                <p id="task-name-show">{{ $tasks->taskType->name }}</p>
                <p>Statut : {{ $tasks->etat }}</p>
                <p>Date : {{  date('d-m-Y', strtotime($tasks->created)) }}</p>

                <div class="container-btn-index-show">
                    <div class="block-btn-dit-show"><a id="task-edit-btn" href="{{ route('tasks.edit', $tasks->id) }}">Modifier</a></div>
                    <div>
                        <form action="{{ route('tasks.destroy', $tasks) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" id="task-delete-btn">Supprimer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="task-btn-return">
    <div><a href="{{ route("tasks.index") }}" class="btn-task-return">Retour</a></div>
</div>
@endsection
