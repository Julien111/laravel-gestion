@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')
@if(session()->has('message'))
    <div class="msg-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('success'))
    <div class="msg-success">
        {{ session()->get('success') }}
    </div>
@endif


<div class="container-page-task-type">
    <h1 id="presentation-tasks">Bonjour, {{ $user->prenom }}</h1>

    <div class="task-block-create">
        <div><a class='task-btn-create' href="{{ route('tasks.create') }}">Ajouter une tâche</a></div>
    </div>
    <table id="table-tasks">
        <tr>
            <th>&Eacute;tat</th>
            <th>Date</th>
            <th>La tâche</th>
            <th>Actions</th>
        </tr>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ $task->etat }}</td>
                <td>{{  date('d-m-Y', strtotime($task->created)) }}</td>
                <td>{{ $task->taskType->name }}</td>
                <td>
                    <div class="container-btn-index">
                        <div class="blocks-btns-index">
                            <div><a id="task-edit-btn" href="{{ route('tasks.edit', $task->id) }}">Modifier</a></div>
                            <div>
                                <form action="{{ route('tasks.destroy', $task) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="task-delete-btn">Supprimer</button>
                                </form>
                            </div>
                        </div>
                        <div id="task-btn-container"><a id="task-show-btn" href="{{ route('tasks.show', $task) }}">Voir plus</a></div>
                    </div>
                </td>

            </tr>
        @endforeach
    </table>
</div>
@endsection
