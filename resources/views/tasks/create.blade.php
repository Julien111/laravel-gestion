@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')

@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif

@if(session()->has('message-error'))
    <div class="msg-error">
        <h4>{{ session()->get('message-error') }}</h4>
    </div>
@endif
<h2 class="title-task">Ajouter votre tâche</h2>

<div class="page-task-form">
<div class="container-form-task">
    <div>
        <form action="{{ route('tasks.store') }}" method="post">
            @csrf
            <div id="form-task">
                <div>
                    <div class="block-form-task">
                        <label for="taskType">Choisir votre tâche :</label>
                        <select name="task_type_id" id="tasks-form-task">
                            @foreach ($taskTypes as $task)
                            <option value="{{ $task->id }}">{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="block-form-task">
                        <label for="etat">Statut de la tâche :</label>
                        <select name="etat" id="tasks-form-etat">
                            <option value="À faire">À faire</option>
                            <option value="En attente">En attente</option>
                            <option value="Terminée">Terminée</option>
                        </select>
                    </div>
                    <div id="task-btn-container">
                        <button type="submit" class="btn-form-task">Créer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="task-btn-return">
    <div><a href="{{ route("tasks.index") }}" class="btn-task-return">Retour</a></div>
</div>
</div>

@endsection
