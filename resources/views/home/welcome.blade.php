@extends('layouts/base')

@section('content')
@include('components-nav.navHome')
<header class="header-home-profil">
    <h1>Laravel gestion</h1>
</header>
<div class="home-block-text">
    <div class="card-text">
        <h2>Organisez votre liste de tâches</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis doloremque facere dolor, eligendi vel sit animi, nulla quaerat quae ea, unde culpa architecto exercitationem et consequuntur eum impedit cumque suscipit.Deserunt velit libero quasi accusamus repudiandae, est esse, ipsum voluptate eum dolorem dolor nobis voluptas at consequatur mollitia doloremque aliquid quod pariatur reiciendis, totam vero eos. Quaerat optio aliquam laudantium.</p>
    </div>
</div>

@endsection
