@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')

<div class="container-profil">
    <div class="profil-block">
        <h2>Vos informations personnelles</h2>
        <p><span>Votre nom :</span> {{ $user->nom }}</p>
        <p><span>Votre prénom :</span> {{ $user->prenom }}</p>
        <p><span>Votre courriel :</span> {{ $user->email }}</p>
    </div>
</div>


@endsection
