@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')
<header class="header-home-profil">
    <h1>Gestion de vos tâches</h1>
</header>

<main class="page-home-profil">
    <div class="text-home-profil">
        <h2>Organisez votre liste de tâches</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis doloremque facere dolor, eligendi vel sit animi, nulla quaerat quae ea, unde culpa architecto exercitationem et consequuntur eum impedit cumque suscipit.Deserunt velit libero quasi accusamus repudiandae, est esse, ipsum voluptate eum dolorem dolor nobis voluptas at consequatur mollitia doloremque aliquid quod pariatur reiciendis, totam vero eos. Quaerat optio aliquam laudantium.</p>
    </div>
    <div class="text-home-profil">
        <h2>Précisez chacune de vos tâches</h2>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, rerum hic nesciunt doloribus totam esse optio nihil possimus, cupiditate inventore dolorum a. Esse sed consequuntur reprehenderit asperiores, numquam tempora eligendi.Laudantium perferendis sint eum itaque dignissimos sunt rem voluptate debitis consequatur accusamus, repudiandae dolore nulla exercitationem veritatis. Eaque, saepe ullam impedit totam ut deleniti incidunt maxime qui unde est consectetur!</p>
    </div>
    <div class="text-home-profil">
        <h2>Lorem Ipsum</h2>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consequuntur, rerum hic nesciunt doloribus totam esse optio nihil possimus, cupiditate inventore dolorum a. Esse sed consequuntur reprehenderit asperiores, numquam tempora eligendi.Laudantium perferendis sint eum itaque dignissimos sunt rem voluptate debitis consequatur accusamus, repudiandae dolore nulla exercitationem veritatis. Eaque, saepe ullam impedit totam ut deleniti incidunt maxime qui unde est consectetur!</p>
    </div>
</main>
@endsection
