@extends('layouts/base')

@section("title", "Ajouter une tâche")

@section('content')

@include('components-nav.navProfil')
<h2 class="title-show">Modifier la tâche</h2>
<div class="container-page-form-task">
<form class="form-task-type" action="{{ route('taskType.update', $taskType) }}" method="post">
    @csrf
    <div class="block-input-task">
        <label for="name">Nouveau nom</label>

        <input id="name" name="name"
        type="text"
        class="@error('name') is-invalid @enderror" value="{{ $taskType->name }}">

        @error('name')
        <div class="alert-error">{{ $message }}</div>
        @enderror
    </div>
    <div class="block-btn-task">
        <div>
            <button type="submit" class="btn-form-task">Modifier</button>
        </div>
    </div>
</form>
</div>
@endsection
