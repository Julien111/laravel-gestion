@extends('layouts/base')

@section("title", "Ajouter une tâche")

@section('content')

@include('components-nav.navProfil')

<div class="show-page-task">
    <h2 class="title-show">Détail</h2>
    <div class="show-container-task">
        <div>
            <p class="show-task">{{ $taskType->name }}</p>
        </div>
        <div class="block-show-task">
            <div><a id="task-edit-btn" href="">Modifier</a></div>
            <div id="task-delete-container"><a id="task-delete-btn" href="">Supprimer</a></div>
        </div>
    </div>
</div>

@endsection
