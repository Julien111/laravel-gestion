@extends('layouts/base')

@section('content')

@include('components-nav.navProfil')
<div class="container-page-task-type">
    <table id="table-tasks">
        <tr>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        @foreach ($taskTypes as $task)
            <tr>
                <td>{{ $task->name }}</td>
                <td>
                    <div class="container-btn-index">
                        <div class="blocks-btns-index">
                            <div><a id="task-edit-btn" href="{{ route('taskType.edit', $task) }}">Modifier</a></div>
                            <div>
                                <form action="{{ route('taskType.destroy',$task) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="task-delete-btn">Supprimer</button>
                                </form>
                            </div>
                        </div>
                        <div id="task-btn-container"><a id="task-show-btn" href="{{ route('taskType.show', $task) }}">Voir plus</a></div>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
<div class="task-block-create">
    <div><a class='task-btn-create' href="{{ route('taskType.create') }}">Ajouter une tâche</a></div>
</div>
@endsection
