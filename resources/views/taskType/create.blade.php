@extends('layouts/base')

@section("title", "Ajouter une tâche")

@section('content')

@include('components-nav.navProfil')
<div class="container-page-form-task">
<form class="form-task-type" action="{{ route('tasks.store') }}" method="post">
    @csrf
    <div class="block-input-task">
        <label for="name">Nouvelle tâche</label>

        <input id="name" name="name"
        type="text"
        class="@error('name') is-invalid @enderror">

        @error('name')
        <div class="alert-error">{{ $message }}</div>
        @enderror
    </div>
    <div class="block-btn-task">
        <div>
            <button type="submit" class="btn-form-task">Créer</button>
        </div>
    </div>
</form>
</div>
@endsection
