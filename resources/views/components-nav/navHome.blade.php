<nav class="font-sans mb-2 flex flex-col text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-slate-300 shadow sm:items-baseline w-full">
  <div class="mb-2 sm:mb-0">
    <a href="{{ route('home') }}" class="text-2xl no-underline text-grey-darkest hover:text-blue-600">Accueil</a>
  </div>
  <div>
    <div class="container-lien-nav">
      <div><a href="{{ route('login') }}" class="text-lg no-underline text-grey-darkest hover:text-blue-500 ml-2">Connexion</a></div>
      <div><a href="{{ route('register') }}" class="text-lg no-underline text-grey-darkest hover:text-blue-500 ml-2">Inscription</a></div>

    </div>
  </div>
</nav>
