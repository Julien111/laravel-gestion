<nav class="font-sans mb-2 flex flex-col text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-slate-300 shadow sm:items-baseline w-full">
  <div class="mb-2 sm:mb-0">
    <a href="{{ route('dashboard') }}" class="text-2xl no-underline text-grey-darkest hover:text-blue-dark">Accueil</a>
  </div>
  <div>
    <div class="container-lien-nav">
      <div><a href="{{ route('tasks.index') }}" class="text-lg no-underline text-grey-darkest hover:text-blue-500 ml-2">Gestion</a></div>
      <div><a href="{{ route('taskType.index') }}" class="text-lg no-underline text-grey-darkest hover:text-blue-500 ml-2">Les tâches</a></div>
      <div><a href="{{ route('profil.index') }}" class="text-lg no-underline text-grey-darkest hover:text-blue-500 ml-2">Profil</a></div>
      <div><form method="POST" action="{{ route('logout') }}">
                @csrf
                <button type="submit" name="logout" class="logout-btn">Déconnexion</button>
             </form>
        </div>
    </div>
  </div>
</nav>
